package com.example.unittestsample.web;

import com.example.unittestsample.domain.Student;
import com.example.unittestsample.service.students.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "/students")
public class StudentController {

    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    DataSource dataSource;

    @GetMapping(value = "/")
    public List<Student> getAllStudents(){
        logger.info("Get all students");
        return studentService.getAllStudents();
        //return new LinkedList<>();
    }

}

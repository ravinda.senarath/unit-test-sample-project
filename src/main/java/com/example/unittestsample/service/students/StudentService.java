package com.example.unittestsample.service.students;

import com.example.unittestsample.domain.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAllStudents();

    Student findStudentByStudentId(String studentId);

    Student updateStudentByStudentId(String studentId, Student student);

    Student createStudent(Student student);

}

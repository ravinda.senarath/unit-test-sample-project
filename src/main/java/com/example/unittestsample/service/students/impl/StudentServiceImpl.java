package com.example.unittestsample.service.students.impl;

import com.example.unittestsample.domain.Student;
import com.example.unittestsample.repository.StudentRepository;
import com.example.unittestsample.service.students.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.getAllStudents();
    }

    @Override
    public Student findStudentByStudentId(String studentId) {
        return null;
    }

    @Override
    public Student updateStudentByStudentId(String studentId, Student student) {
        return null;
    }

    @Override
    public Student createStudent(Student student) {
        return null;
    }
}

package com.example.unittestsample.repository.impl;

import com.example.unittestsample.domain.Student;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<Student> {
    @Nullable
    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        Student student = new Student();
        student.setFirstName(resultSet.getString("FIRST_NAME"));
        return student;
    }
}

package com.example.unittestsample.repository.impl;

import com.example.unittestsample.domain.Student;
import com.example.unittestsample.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Student> getAllStudents() {
        String query = "SELECT * FROM STUDENT";
        return namedParameterJdbcTemplate.query(query, new StudentRowMapper());
    }

    @Override
    public Student findStudentByStudentId(String studentId) {
        return null;
    }

    @Override
    public Student updateStudentByStudentId(String studentId, Student student) {
        return null;
    }

    @Override
    public Student createStudent(Student student) {
        return null;
    }
}

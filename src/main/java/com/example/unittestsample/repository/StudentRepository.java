package com.example.unittestsample.repository;

import com.example.unittestsample.domain.Student;

import java.util.List;

public interface StudentRepository {

    List<Student> getAllStudents();

    Student findStudentByStudentId(String studentId);

    Student updateStudentByStudentId(String studentId, Student student);

    Student createStudent(Student student);

}

package com.example.unittestsample;

import com.example.unittestsample.service.impl.StudentServiceImplTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(Small.class)
@Suite.SuiteClasses(StudentServiceImplTest.class)
public class SmallTestSuite {
}

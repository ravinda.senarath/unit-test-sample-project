package com.example.unittestsample.service.impl;

import com.example.unittestsample.Small;
import com.example.unittestsample.domain.Student;
import com.example.unittestsample.repository.StudentRepository;
import com.example.unittestsample.service.students.impl.StudentServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(Small.class)
public class StudentServiceImplTest {

    @InjectMocks
    private StudentServiceImpl studentService;

    @Mock
    private StudentRepository studentRepositoryMock;

    private List<Student> mockStudents = new LinkedList<>();

    @Before
    public void setup(){
        Student student1 = new Student();
        student1.setFirstName("Test1");
        mockStudents.add(student1);
        Student student2 = new Student();
        mockStudents.add(student2);
    }

    @After
    public void cleanup(){

    }

    @Test
    public void getAllStudentsTest(){
        when(studentRepositoryMock.getAllStudents())
                .thenReturn(mockStudents);
        List<Student> students = studentService.getAllStudents();
        assertThat("Student list should not be null", students, is(notNullValue()));
    }

}
